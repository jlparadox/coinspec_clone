import {Component, Input, OnInit} from '@angular/core';
import {ArticleCard} from '../article-card/article-card.component';

import {HttpService} from '../../../providers/app/http.service';

export interface ArticleList {
  list: ArticleCard[];
}

@Component({
  selector: 'app-article-list',
  templateUrl: 'article-list.component.html'
})
export class ArticleListComponent implements OnInit {

  @Input() data: ArticleList;
  title = "News / Articles";
  obj:any;

  constructor(private httpService:HttpService) {
  }

  ngOnInit() {
    this.getJSONData();
    //this.getSampleData();
  }

  getSampleData(){
    let crypList = ['BTC', 'ETH', 'LTC', 'ETC', 'DOG'];
    let i = 0;
    let itemList:ArticleList = {
        list: [],
    };

    for (let cryp of crypList) {
        let item = new ArticleCard;
        item.title = "Title: " + cryp;
        item.subTitle = "Subtitle: " + cryp;
        item.details = 'details';
        item.image = 'https://placeimg.com/320/240/tech';
        item.description = 'test';
        item.body = 'body test body test body';
        item.url = 'https://en.wikipedia.org/wiki/Standard_test_image';
        item.showFull = false;

        itemList['list'].push(item);
    }

    this.data = itemList;
  }

  getJSONData(){
      let item = new ArticleCard;
      let itemList:ArticleList = {
          list: [],
      };

      this.httpService.getJSON().subscribe(data => {
         let obj = JSON.parse(data);
         for (let res of obj['results']) {
            let item = new ArticleCard;
            item.title = res['Title'];
            item.subTitle = res['Author'];
            item.details = res['Description'];
            item.image = res['Headline_Image'];
            item.description = res['Description'];
            item.body = res['Content_Body'];
            item.url = res['URL'];
            item.showFull = false;
    
            itemList['list'].push(item);  
         }

         this.data = itemList;
      }, error => console.log(error));
  }

  onButtonClicked(item) {

  }
}
