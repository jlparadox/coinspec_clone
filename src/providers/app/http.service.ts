import {Injectable, Component, Input} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class HttpService {

  baseUrl: string;
  assetDataUrl:string = 'assets/data/';
  sampleFileName:string = 'bitcoin_news.articles.json';
  dataFileUrl = this.assetDataUrl + this.sampleFileName;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:3000/';
    let obj;
    this.getJSON();
    //.subscribe(data => {console.log(data)}, error => console.log(error));
  }

  get(url, params): Observable<any> {

    return this.http
      .get(`${this.baseUrl}${url}`, {
        params: params
      });
  }

  public getJSON(){
         return this.http.get(this.dataFileUrl, {responseType: 'text'})
                         //.map((res:any) => JSON.parse(res))
                         .catch((error: any) => {
                           console.log(error)
                           return Observable.throw(new Error(error.status));
                        });
     }
}
